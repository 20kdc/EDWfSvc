# EDWfSvc

## DO NOT USE THIS

THIS IS TERRIBLY OUT OF DATE. EDCD's (EDMC)[https://github.com/EDCD/EDMarketConnector] has a low enough memory usage to replace this.

## DO NOT USE THIS

## Description

EDWfSvc is a simple data collector meant to send data to EDSM & EDDN.

It is written in C++ using Qt framework libraries for a balance between memory usage and portability, allowing usage on Mac OS X (when possible) / Linux DXVK/Wine setups as well as Windows.

The usage is as follows:

```
Linux/Mac OS X using default Wine prefix:
 EDWfSvc "$HOME/.wine/drive_c/users/$USER/Saved Games/Frontier Developments/Elite Dangerous" EDSM_USER EDSM_APIKEY

Windows:
 EDWfSvc "%USERPROFILE%\Saved Games\Frontier Developments\Elite Dangerous" EDSM_USER EDSM_APIKEY
```

If you are switching from another uploader, you may want to move all your journals from the folder before doing this,
 to prevent a bulk re-upload.

Furthermore, please ensure the EDWfKnownJournalData file that is generated is always in the current directory before startup, as this is the 'database' of already uploaded journal data.

EDWfSvc is meant to be a basic data uploader; it is not intended to have a GUI.

Such a thing would increase memory usage, and ultimately be redundant as users tend to also need read access to various services, which must be accessed via browser.

I have literally no idea how Beta universe detection is supposed to work.

DO NOT use this program if you are entering the Beta universe; EDDN/EDSM *will* get mad at both you and me.

Finally, EDDN uploaderId scrambling is implemented.

People who want it removed should first

## Dependencies

Qt 5 Core and Qt 5 Network. May work with Qt 4.

## Build Instructions

1. If possible, use Qt Creator.
2. Failing that, use qmake.
3. Failing that, you'll have to write your own Makefile.

## Flushing the Known Journal Data file

Every time EDWfSvc receives some data from the journal files, it updates a file called "EDWfKnownJournalData".

This file contains pairs of lines, one being the filename, and the other being the decimal size in bytes.

(If a filename line is blank, it is skipped, and the next line is checked for a filename.)

Events that end earlier than or at this point are considered to have been already scanned by EDWfSvc.

If you find your EDWfKnownJournalData file is getting too large:

1. Ensure Elite:Dangerous and EDWfSvc are not running.

2. Move all files out of the journal directory. Do not move them back later, as EDWfSvc will treat them as new files. Consider putting them in a compressed archive, as they are almost certainly thousands of times larger than the EDWfKnownJournalData file, which merely contains information about the growth of these files.

3. Delete EDWfKnownJournalData.

## In Case Of Rejections

Good luck.

## Credits

20kdc for programming.

The EDSM/EDDN community for lots and lots and lots of advice on how to talk to the APIs, and for the servers this program sends data to.

## License

```
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
```

While EDWfSvc itself is CC0 (see COPYING), it needs to dynamically link to Qt5 libraries and potentially libstdc++ (or another C++ standard library) to operate.

The Qt5 libraries are under the LGPL v3 and/or GPL v2 and/or the GPL v3 and/or the Qt commercial usage license as per your specific licensing requirements and permissions, while libstdc++ is licensed under the GPL v3 but specifically granting the GCC Runtime Library Exception.

## Notice

EDWfSvc is not associated with Frontier Developments, EDSM, or EDDN.

No warranty is provided, implied or otherwise, and any mis-transformation of data is not the responsibility of EDWFSvc developers.

Some forks of EDWfSvc may mis-transform data when the main project does not.

The main project may mis-transform data when a fork does not.

How this is to be handled is up to the EDSM and EDDN developers.

Obligatory attempt at a GDPR notice (I'm not a lawyer so I have no idea if this is right):

By running EDWfSvc, you accept that it will upload game journal & market data as per specifications, and that it will locally store tracking data required to avoid re-uploading data.
