/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <QDebug>
#include <QFile>
#include <QDir>
#include "spaceboijsonvalue.h"
#include "journalmonitor.h"
#include "ewsglblu.h"

static const char * scramble1[] = {
    "Om",
    "Blem",
    "Fletch",
    "Tr",

    "Th",
    "Cut",
    "Mix",
    "Bis"
};
static const char * scramble2[] = {
    "cu",
    "er",
    "let",
    "er",

    "et",
    "le",
    "ix",
    "cu"
};
static const char * scramble3[] = {
    "ie",
    "ish",
    "maia",
    "lia",

    "ar",
    "bey",
    "ette",
    "chan"
};
static const char * scramble4[] = {
    " The Awesome",
    " The Insane",
    " The Fun",
    " The Goat",

    " The Kitten",
    " The Bird",
    " The Cat",
    " The Great (and powerful)"
};

static QString scramble(QString & data) {
    int hash = 0;
    for (QChar qc : data) {
        hash += qc.unicode();
        int ex = hash;
        hash <<= 4;
        hash ^= ex;
    }
    QString scrambled = "Scrambled: ";
    scrambled += scramble1[hash & 7];
    hash >>= 3;
    scrambled += scramble2[hash & 7];
    hash >>= 3;
    scrambled += scramble3[hash & 7];
    hash >>= 3;
    scrambled += scramble4[hash & 7];
    return scrambled;
}

JournalMonitor::JournalMonitor(int argc, char ** argv) : QCoreApplication(argc, argv) {
    QStringList qsl = arguments();
    if (qsl.size() != 4) {
        edwfPrintln("By running " EDWF_BRANDFULL ", you accept that data from the journal directory will be sent to EDSM/EDDN on your behalf.");
        edwfPrintln("This is the primary function of the program.");
        edwfPrintln("The program must be passed three arguments: The journal directory folder location, your EDSM username, and your EDSM API key.");
        edwfPrintln("By providing these, you agree to this.");
        timer = nullptr;
        db = nullptr;
        earlyStartupBringToImmediateHalt = true;
    } else {
        edwfPrintln("Starting " EDWF_BRANDFULL);
        journalPath = qsl[1];
        // Must initialize first
        nsys = new NetworkSystem(this);
        edsm = new EDSMInterface(this, nsys, "https://www.edsm.net/api-journal-v1", qsl[2], qsl[3]);
        eddn = new EDDNInterface(this, nsys, "https://eddn.edcd.io:4430/upload/", journalPath + QDir::separator(), scramble(qsl[3]));
        db = new JournalDB(this, EDWF_JOURNALDB);
        timer = new QTimer(this);
        timer->setSingleShot(true);
        // These connections stay throughout the run.
        connect(edsm, SIGNAL(readyForFirstUpload()), this, SLOT(update()));
        connect(edsm, SIGNAL(readyForNextUpload()), this, SLOT(startTimer()));
        connect(timer, SIGNAL(timeout()), this, SLOT(update()));

        // Connect incoming ED events into the outgoing interfaces.
        connect(db, SIGNAL(onEvent(SpaceBoiJsonValue &, EDSMTransientTagger &)), edsm, SLOT(incomingEvent(SpaceBoiJsonValue &, EDSMTransientTagger &)));
        connect(db, SIGNAL(onEvent(SpaceBoiJsonValue &, EDSMTransientTagger &)), eddn, SLOT(incomingEvent(SpaceBoiJsonValue &, EDSMTransientTagger &)));

        earlyStartupBringToImmediateHalt = false;
    }
}

void JournalMonitor::startTimer() {
    qDebug("In idle state successfully.");
    timer->start(30000);
}

void JournalMonitor::update() {
    QDir dir(journalPath);
    QStringList iList = dir.entryList(QDir::Filter::Files);
    iList.sort(Qt::CaseSensitivity::CaseInsensitive);
    for (QString & name : iList) {
        // Potentially valid log file. Pass to JournalDB for further processing,
        //  since it knows which files have already been 'seen'.
        // It sends events back with the required data. Any caching is black-boxed out.
        if (name.startsWith("Journal.", Qt::CaseSensitivity::CaseInsensitive) && name.endsWith(".log", Qt::CaseSensitivity::CaseInsensitive))
            db->processLog(name, journalPath + QDir::separator() + name);
    }
    edsm->flush();
}
