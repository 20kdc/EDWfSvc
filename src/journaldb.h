/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef JOURNALDB_H
#define JOURNALDB_H

#include <QHash>
#include <QTextStream>
#include <QObject>
#include <QFileInfo>
#include <QFile>

#include "edsmtransienttagger.h"
#include "edsminterface.h"
#include "spaceboijsonvalue.h"

typedef struct {
    qint64 size, lines;
} JournalDBKnownJournalFile;

/*
 * Responsible for reliably outputting all events EDWFSvc has not handled yet in order.
 */
class JournalDB : public QObject {
    Q_OBJECT
public:
    explicit JournalDB(QObject *parent, const QString & db);
    // Inputs a file into the DB.
    void processLog(const QString & id, const QString & journal);
signals:
    void onEvent(SpaceBoiJsonValue & obj, EDSMTransientTagger & tagger);
private:

    // This contains information on all journal files, and their sizes.
    // Should be the only actual database of journal data.
    // Loaded from the file writeKnown appends to.
    QHash<QString, JournalDBKnownJournalFile> knownJournalFiles;
    // Used for the only form of internal storage JournalDB uses.
    QFile knownRepository;
};

#endif // JOURNALDB_H
