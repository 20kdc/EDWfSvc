/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <QJsonDocument>
#include <QJsonArray>
#include <QFile>
#include <QTextStream>
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>

#include "edsminterface.h"
#include "ewsglblu.h"

EDSMInterface::EDSMInterface(QObject *parent, NetworkSystem * nsys, const QString & edsmApiRoot, const QString & edsmApiName, const QString & edsmApiKey) : QObject(parent), buffer("{", QList<SpaceBoiJsonValue>()) , bufferMessage("[", QList<SpaceBoiJsonValue>()) {
    ns = nsys;

    apiRoot = edsmApiRoot;
    // Hard-code some safety.
    discardable.insert("\"Friends");
    discardable.insert("\"SendText");
    discardable.insert("\"ReceiveText");

    ns->get(edsmApiRoot + "/discard", nullptr, this, SLOT(onDiscardReply(void *, bool, QByteArray &)));
    QNetworkRequest rqRequest(edsmApiRoot + "/discard");

    buffer.setIV("\"commanderName", SpaceBoiJsonValue("\"" + edsmApiName));
    buffer.setIV("\"apiKey", SpaceBoiJsonValue("\"" + edsmApiKey));
    buffer.setIV("\"fromSoftware", SpaceBoiJsonValue("\"" EDWF_BRAND));
    buffer.setIV("\"fromSoftwareVersion", SpaceBoiJsonValue("\"" EDWF_VERSION));
}

void EDSMInterface::onDiscardReply(void * userdata, bool success, QByteArray & response) {
    if (!success) {
        edwfPrintln("Failed to receive Discard information.");
    } else {
        edwfPrintln("Received Discard information.");
        QJsonArray qjd = QJsonDocument::fromJson(response).array();
        for (QJsonValue qjv : qjd)
            discardable.insert("\"" + qjv.toString());
    }
    readyForFirstUpload();
}
void EDSMInterface::onSendReply(void * userdata, bool success, QByteArray & response) {
    readyForNextUpload();
}

void EDSMInterface::incomingEvent(SpaceBoiJsonValue & qjo, EDSMTransientTagger & tagger) {
    if (discardable.contains(qjo["event"].objectToken))
        return;

    SpaceBoiJsonValue clone = qjo;
    tagger.tagEDSM(clone);
    bufferMessage.contents.append(clone);
}

void EDSMInterface::flush() {
    if (bufferMessage.contents.size() == 0) {
        readyForNextUpload();
        return;
    }
    edwfPrintln(QString("Submitting ") + QString::number(bufferMessage.contents.size()) + " EDSM events...");

    buffer.setIV("\"message", bufferMessage);
    bufferMessage.contents.clear();

    ns->postJson(apiRoot, buffer, nullptr, this, SLOT(onSendReply(void *, bool, QByteArray &)));
}
