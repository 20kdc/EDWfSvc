/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QDir>
#include "eddninterface.h"
#include "ewsglblu.h"

EDDNInterface::EDDNInterface(QObject * parent, NetworkSystem * nsys, const QString & eddnApiRoot, const QString & oRoot, const QString & who) : QObject(parent), whoami(who), apiRoot(eddnApiRoot), jsonPrefix(oRoot) {
    ns = nsys;
}

void EDDNInterface::send(const SpaceBoiJsonValue & data) {
    ns->postJson(apiRoot, data, nullptr, nullptr, nullptr);
}

static SpaceBoiJsonValue initEDDN(SpaceBoiJsonValue & msg, const char * sRef, QString whoami) {
    SpaceBoiJsonValue ws = SpaceBoiJsonValue::Object;
    SpaceBoiJsonValue obj = SpaceBoiJsonValue::Object;
    ws.setIV("\"uploaderID", SpaceBoiJsonValue("\"" + whoami));
    ws.setIV("\"softwareName", SpaceBoiJsonValue("\"" EDWF_BRAND));
    ws.setIV("\"softwareVersion", SpaceBoiJsonValue("\"" EDWF_VERSION));
    obj.setIV("\"$schemaRef", SpaceBoiJsonValue(QString("\"") + sRef));
    obj.setIV("\"header", ws);
    obj.setIV("\"message", msg);
    return obj;
}

static SpaceBoiJsonValue convertMarketItem(const SpaceBoiJsonValue & marketItem) {
    SpaceBoiJsonValue res = SpaceBoiJsonValue::Object;
    QString nam = marketItem["\"Name"].objectToken;
    res.setIV("\"name", SpaceBoiJsonValue("\"" + nam.mid(2, nam.size() - 8)));
    res.setIV("\"meanPrice", marketItem["\"MeanPrice"]);
    res.setIV("\"buyPrice", marketItem["\"BuyPrice"]);
    res.setIV("\"stock", marketItem["\"Stock"]);
    res.setIV("\"stockBracket", marketItem["\"StockBracket"]);
    res.setIV("\"sellPrice", marketItem["\"SellPrice"]);
    res.setIV("\"demand", marketItem["\"Demand"]);
    res.setIV("\"demandBracket", marketItem["\"DemandBracket"]);
    SpaceBoiJsonValue dataSF = SpaceBoiJsonValue::Array;
    if (marketItem["\"Consumer"].objectToken == "true")
        dataSF.contents.append(SpaceBoiJsonValue("\"Consumer"));
    if (marketItem["\"Producer"].objectToken == "true")
        dataSF.contents.append(SpaceBoiJsonValue("\"Producer"));
    if (marketItem["\"Rare"].objectToken == "true")
        dataSF.contents.append(SpaceBoiJsonValue("\"Rare"));
    if (dataSF.contents.size() > 0)
        res.setIV("\"statusFlags", dataSF);
    return res;
}
static SpaceBoiJsonValue convertMarketEconomy(const SpaceBoiJsonValue & marketItem) {
    SpaceBoiJsonValue res = SpaceBoiJsonValue::Object;
    QString nam = marketItem["\"Name"].objectToken;
    res.setIV("\"name", SpaceBoiJsonValue("\"" + nam.mid(10, nam.size() - 11)));
    res.setIV("\"proportion", marketItem["\"Proportion"]);
    return res;
}
static void convertMarket(EDDNInterface * eddn, SpaceBoiJsonValue & data, EDSMTransientTagger & tagger) {
    SpaceBoiJsonValue data2 = SpaceBoiJsonValue::Object;
    SpaceBoiJsonValue data3 = SpaceBoiJsonValue::Array;
    for (SpaceBoiJsonValue & marketItem : data["\"Items"].contents)
        data3.contents.append(convertMarketItem(marketItem));
    SpaceBoiJsonValue data4 = SpaceBoiJsonValue::Null;
    if (!tagger.stationEconomies.null()) {
        data4 = SpaceBoiJsonValue::Array;
        for (SpaceBoiJsonValue & economy : tagger.stationEconomies.contents)
            data4.contents.append(convertMarketEconomy(economy));
    }
    data2.setIV("\"systemName", data["\"StarSystem"]);
    data2.setIV("\"timestamp", data["\"timestamp"]);
    data2.setIV("\"stationName", data["\"StationName"]);
    data2.setIV("\"marketId", data["\"MarketID"]);
    data2.setIV("\"commodities", data3);
    if (!data4.null())
        data2.setIV("\"economies", data4);
    eddn->send(initEDDN(data2, "https://eddn.edcd.io/schemas/commodity/3", eddn->whoami));
}
static void convertShipyard(EDDNInterface * eddn, SpaceBoiJsonValue & data, EDSMTransientTagger & tagger) {
    SpaceBoiJsonValue data2 = SpaceBoiJsonValue::Object;
    data2.setIV("\"systemName", data["\"StarSystem"]);
    data2.setIV("\"timestamp", data["\"timestamp"]);
    data2.setIV("\"stationName", data["\"StationName"]);
    data2.setIV("\"marketId", data["\"MarketID"]);
    SpaceBoiJsonValue data3 = SpaceBoiJsonValue::Array;
    for (SpaceBoiJsonValue & marketItem : data["\"PriceList"].contents)
        data3.contents.append(marketItem["\"ShipType"]);
    data2.setIV("\"ships", data3);
    eddn->send(initEDDN(data2, "https://eddn.edcd.io/schemas/shipyard/2", eddn->whoami));
}
static void convertOutfitting(EDDNInterface * eddn, SpaceBoiJsonValue & data, EDSMTransientTagger & tagger) {
    SpaceBoiJsonValue data2 = SpaceBoiJsonValue::Object;
    data2.setIV("\"systemName", data["\"StarSystem"]);
    data2.setIV("\"timestamp", data["\"timestamp"]);
    data2.setIV("\"stationName", data["\"StationName"]);
    data2.setIV("\"marketId", data["\"MarketID"]);
    SpaceBoiJsonValue data3 = SpaceBoiJsonValue::Array;
    for (SpaceBoiJsonValue & marketItem : data["\"Items"].contents) {
        QString effectiveString = marketItem["\"Name"].objectToken.toLower();
        /* "
         * You can just uppercase the first letter
         * Onmy side I'm lowercasing everything anyway
         * "
         * - Anthor, https://discordapp.com/channels/156642424339300352/393758245472174081/528285531633745960
         */
        if (effectiveString.startsWith("\"hpt_")) {
            effectiveString = "\"Hpt_" + effectiveString.mid(5);
        } else if (effectiveString.startsWith("\"int_")) {
            effectiveString = "\"Int_" + effectiveString.mid(5);
        } else if (effectiveString.contains("_armour_")) {
            effectiveString = effectiveString.replace("_armour_", "_Armour_");
        } else {
            continue;
        }
        data3.contents.append(SpaceBoiJsonValue(effectiveString));
    }
    data2.setIV("\"modules", data3);
    eddn->send(initEDDN(data2, "https://eddn.edcd.io/schemas/outfitting/2", eddn->whoami));
}

// expectedTimestamp is in objectToken form.
static void runEDDN(const QString & baseName, QString expectedTimestamp, void (*callback)(EDDNInterface * eddn, SpaceBoiJsonValue & coredata, EDSMTransientTagger & tagger), EDDNInterface * eddn, EDSMTransientTagger & tagger) {
    QFile qf(baseName);
    qf.open(QFile::ReadOnly);
    const char * err;
    QByteArray qba = qf.readAll();
    SpaceBoiJsonValue qjd = SpaceBoiJsonValue::parse(qba, &err);
    qf.close();
    if (!err) {
        if (qjd["\"timestamp"] == expectedTimestamp) {
            edwfPrintln("EDDN: " + baseName + "@" + expectedTimestamp);
            callback(eddn, qjd, tagger);
        } else {
            edwfPrintln("Got prepared for EDDN handling: " + baseName + " but encountered time distortion");
        }
    } else {
        edwfPrintln("Got prepared for EDDN handling: " + baseName + " but encountered JSON parse error: " + err);
    }
}

void EDDNInterface::incomingEvent(SpaceBoiJsonValue & qjo, EDSMTransientTagger & tagger) {
    SpaceBoiJsonValue evName = qjo["\"event"];
    if (evName == "\"Outfitting") {
        // Convert & upload Outfitting object.
        runEDDN(jsonPrefix + "Outfitting.json", qjo["\"timestamp"].objectToken, convertOutfitting, this, tagger);
    } else if (evName == "\"Shipyard") {
        // Convert & upload Shipyard object.
        runEDDN(jsonPrefix + "Shipyard.json", qjo["\"timestamp"].objectToken, convertShipyard, this, tagger);
    } else if (evName == "\"Market") {
        // Convert & upload Market object.
        runEDDN(jsonPrefix + "Market.json", qjo["\"timestamp"].objectToken, convertMarket, this, tagger);
    } else if (evName == "\"MarketSell") {
        // Determine if this qualifies for a BlackMarket schema upload
        // { "timestamp":"2018-12-25T21:57:24Z", "event":"MarketSell", "MarketID":3221917440,
        //   "Type":"hydrogenfuel", "Type_Localised":"Hydrogen Fuel", "Count":2, "SellPrice":60,
        //   "TotalSale":120, "AvgPricePaid":0, "StolenGoods":true, "BlackMarket":true }
        /* NOT HAPPENING UNTIL PROHIBITED HAS A VALID SOURCE. - 20kdc
        if (qjo["\"BlackMarket"] == "true") {
            SpaceBoiJsonValue copy = SpaceBoiJsonValue::Object;
            copy.setIV("\"systemName", tagger.systemName);
            copy.setIV("\"stationName", tagger.stationName);
            copy.setIV("\"marketId", qjo["\"MarketID"]);
            copy.setIV("\"timestamp", qjo["\"timestamp"]);
            copy.setIV("\"name", qjo["\"Type"]);
            copy.setIV("\"sellPrice", qjo["\"SellPrice"]);
            copy.setIV("\"prohibited", qjo["\""]);
            copy.rmLocalised();
            send(initEDDN(copy, "https://eddn.edcd.io/schemas/blackmarket/1", whoami));
        }*/
    } else if ((evName == "\"Docked") || (evName == "\"FSDJump") || (evName == "\"Scan") || (evName == "\"Location")) {
        SpaceBoiJsonValue copy = qjo;
        copy.setIV("\"StarSystem", tagger.systemName);
        copy.setIV("\"StarPos", tagger.systemCoordinates);
        copy.setIV("\"SystemAddress", tagger.systemAddress);
        copy.rmLocalised();
        send(initEDDN(copy, "https://eddn.edcd.io/schemas/journal/1", whoami));
    }
}
