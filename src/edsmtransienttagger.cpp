/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include "edsmtransienttagger.h"

EDSMTransientTagger::EDSMTransientTagger(QObject *parent) : QObject(parent) {

}

// NOTE: It may be an idea to check this with the EDSM server source code,
//  rather than the official documentation, as the EDSM server performs manual state tracking
//  for entire-journal loads anyway, and furthermore,
//  the code that writes 'cmdr' is clearly NOT PRESENT there.
void EDSMTransientTagger::accept(SpaceBoiJsonValue & obj) {
    SpaceBoiJsonValue ev = obj["\"event"];
    if (ev.null())
        return;

    if (ev == "\"LoadGame") {
        resetState();
        cmdr = obj["\"Commander"];
    } else if (ev == "\"NewCommander") {
        resetState();
        cmdr = obj["\"Name"];
    } else if ((ev == "\"SupercruiseEntry") || (ev == "\"Undocked")) {
        // Cannot possibly be docked in either of these circumstances. Applying to SupercruiseEntry is Bravada Cadelanne's idea.
        marketId = stationName = SpaceBoiJsonValue::Null;
    } else if (ev == "\"ShipyardBuy") {
        shipId = SpaceBoiJsonValue::Null;
    } else if ((ev == "\"SetUserShipName") || (ev == "\"ShipyardSwap") || (ev == "\"Loadout")) {
        shipId = obj["\"ShipID"];
    } else if ((ev == "\"Location") || (ev == "\"FSDJump") || (ev == "\"Docked")) {
        // These are magical non-existent star systems used to either make the game itself happy
        //  or to raise big error flags should events from here get into Milky-Way.
        // We *may* want to disable event upload during CQC; official spec implies "don't".
        SpaceBoiJsonValue starSystem = obj["\"StarSystem"];

        bool equestria = (starSystem == "\"ProvingGround") || (starSystem == "\"CQC");
        if (equestria) {
            systemAddress = systemName = systemCoordinates = marketId = stationName = stationEconomies = SpaceBoiJsonValue::Null;
        } else {
            systemName = obj["\"StarSystem"];

            systemAddress = obj["\"SystemAddress"];
            if (systemAddress != "null") {
                // Check if system coordinates should be nulled out by comparing system address.
                if (systemAddress != obj["\"SystemAddress"].objectToken)
                    systemCoordinates = SpaceBoiJsonValue::Null;
            } else {
                // Always null it out for safety.
                systemCoordinates = SpaceBoiJsonValue::Null;
            }

            // I don't like the likelihood of getting out of sync with StarSystem, so I'm again
            //  ignoring the official API recommendations on the basis of that:
            //  *less but more reliable* information > *more but less reliable* information.
            systemAddress = obj["\"SystemAddress"];
            // Above code controls if this gets nulled out on StarPos non-presence. This is Bravada Cadelanne's idea.
            obj.hasIV("\"StarPos", &systemCoordinates);

            marketId = obj["\"MarketID"];
            stationName = obj["\"StationName"];
            stationEconomies = obj["\"StationEconomies"];
        }
    } else if (ev == "\"JoinACrew") {
        resetState();
        sendEvents = obj["\"Captain"] == cmdr.objectToken;
    } else if (ev == "\"LeaveACrew") {
        // shipId here isn't in line with protocol.
        resetState();
    }
}

void EDSMTransientTagger::tagEDSM(SpaceBoiJsonValue & obj) {
    obj.setIV("\"_systemAddress", systemAddress);
    obj.setIV("\"_systemName", systemName);
    obj.setIV("\"_systemCoordinates", systemCoordinates);
    obj.setIV("\"_marketId", marketId);
    obj.setIV("\"_stationName", stationName);
    obj.setIV("\"_shipId", shipId);
}

void EDSMTransientTagger::resetState() {
    // NOTE: shipId here isn't in line with protocol desc. under a few circumstances,
    //  but it looks like shipId could be stale if this isn't done.
    // Allow EDSM to reject based on lack of information over possibly incorrect information.
    systemAddress = systemName = systemCoordinates = marketId = stationName = shipId = stationEconomies = SpaceBoiJsonValue::Null;
}
