/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <QStringList>
#include <QStringRef>
#include "spaceboijsonvalue.h"
#include "ewsglblu.h"

SpaceBoiJsonValue SpaceBoiJsonValue::Null = SpaceBoiJsonValue(QString("null"));
SpaceBoiJsonValue SpaceBoiJsonValue::True = SpaceBoiJsonValue(QString("true"));
SpaceBoiJsonValue SpaceBoiJsonValue::False = SpaceBoiJsonValue(QString("false"));
SpaceBoiJsonValue SpaceBoiJsonValue::Object = SpaceBoiJsonValue("{", QList<SpaceBoiJsonValue>());
SpaceBoiJsonValue SpaceBoiJsonValue::Array = SpaceBoiJsonValue("[", QList<SpaceBoiJsonValue>());

SpaceBoiJsonValue::SpaceBoiJsonValue(const QString & primaryToken) : objectToken(primaryToken) {

}
SpaceBoiJsonValue::SpaceBoiJsonValue(const SpaceBoiJsonValue & l, const SpaceBoiJsonValue & r) : objectToken(":") {
    contents.append(l);
    contents.append(r);
}
SpaceBoiJsonValue::SpaceBoiJsonValue(const QString & primaryToken, const QList<SpaceBoiJsonValue> & content) : objectToken(primaryToken), contents(content) {

}

#define SBJVCLAZZ_EOF 1
#define SBJVCLAZZ_BREAKER 2
#define SBJVCLAZZ_STRING 4
#define SBJVCLAZZ_STRINGESCAPE 8
#define SBJVCLAZZ_WS 16
#define SBJVCLAZZ_UDIG 32
// Occupies 4 bits for value
#define SBJVCLAZZ_UDIGVB 64

// Root state. Eats whitespace (incl. EOF).
#define SBJVSTATE_ROOT 0
#define SBJVSTATE_TOKEN 1
#define SBJVSTATE_STRING 2
#define SBJVSTATE_STRINGESCAPE 3
#define SBJVSTATE_STRINGESCAPEU1 4
#define SBJVSTATE_STRINGESCAPEU2 5
#define SBJVSTATE_STRINGESCAPEU3 6
#define SBJVSTATE_STRINGESCAPEU4 7

class sbjvstate_t {
public:
    int state = 0;
    int uBuild = 0;
    QString buildingString = "";
    QStringList building = QStringList();
    QChar stringChar = 0;
};

// Shutdown occurs when clazz & SBJVCLAZZ_EOF is true and this has been executed.
static void stateMachine(sbjvstate_t * machine, QChar res, int clazz) {
    while (true) {
        // edwfPrintln(QString::asprintf("%c %i", res.cell(), machine->state));
        if (machine->state == SBJVSTATE_ROOT) {
            if (clazz & SBJVCLAZZ_WS) {
                return;
            } else if (clazz & SBJVCLAZZ_BREAKER) {
                machine->building.append(res);
                return;
            } else if (clazz & SBJVCLAZZ_STRING) {
                machine->buildingString = "\"";
                machine->stringChar = res;
                machine->state = SBJVSTATE_STRING;
                return;
            } else {
                machine->state = SBJVSTATE_TOKEN;
                machine->buildingString = "";
                // Don't return, this char is part of the token.
            }
        } else if (machine->state == SBJVSTATE_TOKEN) {
            if (clazz & (SBJVCLAZZ_BREAKER | SBJVCLAZZ_WS)) {
                machine->building.append(machine->buildingString);
                // Don't return, pass to root
                machine->state = SBJVSTATE_ROOT;
            } else {
                machine->buildingString += res;
                return;
            }
        } else if (machine->state == SBJVSTATE_STRING) {
            if (res == machine->buildingString[0]) {
                machine->building.append(machine->buildingString);
                machine->state = SBJVSTATE_ROOT;
                return;
            } else if (clazz & SBJVCLAZZ_STRINGESCAPE) {
                machine->state = SBJVSTATE_STRINGESCAPE;
                return;
            } else {
                machine->buildingString += res;
                return;
            }
        } else if (machine->state == SBJVSTATE_STRINGESCAPE) {
            machine->state = SBJVSTATE_STRING;
            if (res == 'u') {
                machine->state = SBJVSTATE_STRINGESCAPEU1;
                machine->uBuild = 0;
            } else if (res == 'b') {
                machine->buildingString += '\b';
            } else if (res == 'f') {
                machine->buildingString += '\f';
            } else if (res == 'n') {
                machine->buildingString += '\n';
            } else if (res == 'r') {
                machine->buildingString += '\r';
            } else if (res == 't') {
                machine->buildingString += '\t';
            } else {
                machine->buildingString += res;
            }
            return;
        } else if ((machine->state >= SBJVSTATE_STRINGESCAPEU1) && (machine->state <= SBJVSTATE_STRINGESCAPEU4)) {
            machine->uBuild <<= 4;
            machine->uBuild |= (clazz / SBJVCLAZZ_UDIGVB) & 0xF;
            if (machine->state == SBJVSTATE_STRINGESCAPEU4) {
                machine->buildingString += QChar(machine->uBuild);
                machine->state = SBJVSTATE_STRING;
            } else {
                machine->state++;
            }
            return;
        }
    }
}

static int classify(QChar qch) {
    if (qch <= 32)
        return SBJVCLAZZ_WS;
    switch (qch.unicode()) {
        case '[':
        case ']':
        case ':':
        case '{':
        case '}':
        case ',':
            return SBJVCLAZZ_BREAKER;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            return (SBJVCLAZZ_UDIG | (SBJVCLAZZ_UDIGVB * (qch.unicode() - '0')));
        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
            return SBJVCLAZZ_UDIG | (SBJVCLAZZ_UDIGVB * (0xA + ((qch.unicode() - 'A') & 0x1F)));
        case '"':
        case '\'':
            return SBJVCLAZZ_STRING;
        case '\\':
            return SBJVCLAZZ_STRINGESCAPE;
    }
    return 0;
}

static QStringList tokenizeJSON(QString & input, const char ** okay) {
    sbjvstate_t state;
    int idx = 0;
    int iSize = input.size();
    while (idx < iSize) {
        QChar qch = input[idx++];
        stateMachine(&state, qch, classify(qch));
    }
    stateMachine(&state, 0, SBJVCLAZZ_WS | SBJVCLAZZ_EOF);
    if (state.state != SBJVSTATE_ROOT)
        *okay = "Ended after EOF in non-root state (early EOF)";
    return state.building;
}

static SpaceBoiJsonValue parseFromTokensOuter(QStringList & tkns, int & index, const char ** err);

static SpaceBoiJsonValue parseFromTokensInner(QStringList & tkns, int & index, const char ** err) {
    if (index < tkns.size()) {
        QString tkn = tkns[index++];
        if ((tkn == "{") || (tkn == "[")) {
            const char * ender = "}";
            if (tkn == "[")
                ender = "]";
            QList<SpaceBoiJsonValue> content;
            bool allowedMore = true;
            while (index < tkns.size()) {
                if ((tkns[index] == "]") || (tkns[index] == "}")) {
                    if (tkns[index] != ender)
                        *err = "Ended with unexpected token type.";
                    index++;
                    break;
                }
                if (!allowedMore)
                    *err = "Not allowed to continue, but continued.";
                content.append(parseFromTokensOuter(tkns, index, err));
                allowedMore = false;
                if (tkns[index] == ",") {
                    index++;
                    allowedMore = true;
                }
            }
            return SpaceBoiJsonValue(tkn, content);
        } else if ((tkn == ":") || (tkn == "]") || (tkn == "}")) {
            index--;
            // Leave to error
        } else {
            return SpaceBoiJsonValue(tkn);
        }
    }
    *err = "Early cancellation";
    return SpaceBoiJsonValue("undefined");
}

static SpaceBoiJsonValue parseFromTokensOuter(QStringList & tkns, int & index, const char ** err) {
    SpaceBoiJsonValue main = parseFromTokensInner(tkns, index, err);
    if (index < tkns.size()) {
        if (tkns[index] == ":") {
            index++;
            return SpaceBoiJsonValue(main, parseFromTokensOuter(tkns, index, err));
        }
    }
    return main;
}

SpaceBoiJsonValue SpaceBoiJsonValue::parse(QByteArray & json, const char ** err) {
    *err = nullptr;
    QString qs = QString::fromUtf8(json);
    QStringList tkns = tokenizeJSON(qs, err);
    int index = 0;
    return parseFromTokensOuter(tkns, index, err);
}

void SpaceBoiJsonValue::writeString(QString &basis) const {
    if (objectToken.startsWith("\"")) {
        // Escape string...
        int sz = objectToken.size();
        basis += '\"';
        for (int i = 1; i < sz; i++) {
            QChar max = objectToken[i];
            if ((max == '\"') || (max == '\\') || (max == '/')) {
                basis += '\\';
                basis += max;
            } else if (max == '\b') {
                basis += "\\b";
            } else if (max == '\f') {
                basis += "\\f";
            } else if (max == '\n') {
                basis += "\\n";
            } else if (max == '\r') {
                basis += "\\r";
            } else if (max == '\t') {
                basis += "\\t";
            } else if (max < 32) {
                basis += "\\u";
                QString qs;
                qs.setNum(max.unicode(), 16);
                while (qs.size() < 4)
                    qs = "0" + qs;
                basis += qs;
            } else {
                basis += max;
            }
        }
        basis += '\"';
    } else if (objectToken == ":") {
        contents[0].writeString(basis);
        basis += ":";
        contents[1].writeString(basis);
    } else if ((objectToken == "{") || (objectToken == "[")) {
        basis += objectToken;
        bool first = true;
        for (const SpaceBoiJsonValue & sbjv : contents) {
            if (!first)
                basis += ",";
            first = false;
            sbjv.writeString(basis);
        }
        if (objectToken == "{") {
            basis += "}";
        } else {
            basis += "]";
        }
    } else {
        basis += objectToken;
    }
}

QByteArray SpaceBoiJsonValue::write() const {
    QString ws;
    writeString(ws);
    return ws.toUtf8();
}

void SpaceBoiJsonValue::rmLocalised() {
    if (objectToken != "{")
        return;
    int contentsSize = contents.size();
    for (int i = 0; i < contentsSize; i++) {
        // Do not borrow; data may be removed
        SpaceBoiJsonValue kv = contents[i];
        if (kv.objectToken != ":")
            continue;
        QString kvt = kv.contents[0].objectToken;
        if (kvt.endsWith("_Localised") ||
                kvt == "\"CockpitBreach" ||
                kvt == "\"BoostUsed" ||
                kvt == "\"FuelLevel" ||
                kvt == "\"FuelUsed" ||
                kvt == "\"JumpDist" ||
                kvt == "\"Latitude" ||
                kvt == "\"Longitude" ||
                kvt == "\"Latitude" ||
                kvt == "\"MyReputation" ||
                kvt == "\"PlayerFaction" ||
                kvt == "\"HappiestSystem") {
            contents.removeAt(i);
            i--;
            contentsSize--;
        } else {
            kv.contents[1].rmLocalised();
        }
    }
}
