/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef JOURNALMONITOR_H
#define JOURNALMONITOR_H

#include <QString>
#include <QCoreApplication>
#include <QTextStream>
#include <QTimer>

#include "journaldb.h"
#include "edsminterface.h"
#include "eddninterface.h"
#include "networksystem.h"

/*
 * The application. Ties the various different ongoing async tasks together.
 * EDSM is the only interface where ordering concerns are major, so the system waits on it.
 * EDDN is handled as events go through the system (incomingEvent).
 */
class JournalMonitor : public QCoreApplication {
    Q_OBJECT
public:
    JournalMonitor(int argc, char ** argv);
    bool earlyStartupBringToImmediateHalt;
private slots:
    void startTimer();
    void update();
private:
    QString journalPath;
    QTimer * timer;
    EDSMInterface * edsm;
    EDDNInterface * eddn;
    NetworkSystem * nsys;
    JournalDB * db;
};

#endif // JOURNALMONITOR_H
