/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <QFileDevice>
#include <QJsonDocument>
#include "journaldb.h"
#include "ewsglblu.h"
#include "spaceboijsonvalue.h"

JournalDB::JournalDB(QObject *parent, const QString & db) : QObject(parent), knownRepository(db) {
    if (knownRepository.open(QFile::ReadOnly)) {
        QTextStream qts(&knownRepository);
        QStringList lines = qts.readAll().split('\n');
        QString bufferA, bufferB;
        int waiting = 0;
        for (QString & qs : lines) {
            if (waiting == 0) {
                bufferA = qs;
                // Empty lines are assumed to be due to accidental breakage.
                if (bufferA.isEmpty())
                    waiting = -1;
            } else if (waiting == 1) {
                bufferB = qs;
            } else if (waiting == 2) {
                knownJournalFiles[bufferA] = { bufferB.toLong(), qs.toLong() };
            }
            waiting = (waiting + 1) % 3;
        }
        knownRepository.close();
    }
    knownRepository.open(QFile::Append | QFile::WriteOnly);
}

void JournalDB::processLog(const QString & id, const QString & fPath) {
    QFile qf(fPath);

    JournalDBKnownJournalFile edsmKnown = {0, 0};
    qint64 size = qf.size();
    if (knownJournalFiles.contains(id))
        edsmKnown = knownJournalFiles[id];
    if (edsmKnown.size >= size)
        return;

    // It has now been confirmed that new events are available.

    if (!qf.open(QIODevice::ReadOnly)) {
        edwfPrintln("NoRead: " + id);
        return;
    } else {
        edwfPrintln("Update: " + id);
    }
    QTextStream qts(&qf);

    EDSMTransientTagger tagger;
    QStringList lines = qts.readAll().split('\n');
    qint64 newLines = 0;
    qint64 lineIndex = 0;
    for (QString & line : lines) {
        QByteArray data = line.toUtf8();

        const char * err;
        SpaceBoiJsonValue sbjv = SpaceBoiJsonValue::parse(data, &err);
        // edwfPrintln(QString(parseOkay) + " @ " + sbjv.write());

        if (!err) {
            tagger.accept(sbjv);
            // Tag + send if the position *after* the event
            //  is after the last position successfully gotten to previously.
            // "This was the last event read and there was no corrupted data after" would be ==
            if (tagger.sendEvents && (lineIndex >= edsmKnown.lines))
                emit onEvent(sbjv, tagger);
            newLines = lineIndex + 1;
        } else {
            edwfPrintln(err);
        }
        lineIndex++;
    }

    SpaceBoiJsonValue example = SpaceBoiJsonValue::Object;
    tagger.tagEDSM(example);
    edwfPrintln("Status: " + example.writeString());

    knownJournalFiles[id] = {
        size,
        newLines
    };
    // If pretending, become forgetful.
#ifndef DEBUG_FORGETFUL
    QTextStream writeKnown(&knownRepository);
    writeKnown << id << '\n' << qf.pos() << '\n' << newLines << '\n';
#endif
}
