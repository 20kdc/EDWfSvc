/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef EDDNINTERFACE_H
#define EDDNINTERFACE_H

#include <QObject>
#include "networksystem.h"

#include "edsmtransienttagger.h"

class EDDNInterface : public QObject {
    Q_OBJECT
public:
    explicit EDDNInterface(QObject *parent, NetworkSystem * nsys, const QString & eddnApiRoot, const QString & jsonPrefix, const QString & whoami);
    void send(const SpaceBoiJsonValue & data);
    QString whoami;
public slots:
    void incomingEvent(SpaceBoiJsonValue & qjo, EDSMTransientTagger & tagger);
private:
    NetworkSystem * ns;
    QString apiRoot, jsonPrefix;
};

#endif // EDDNINTERFACE_H
