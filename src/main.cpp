/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <journalmonitor.h>

int main(int argc, char *argv[]) {
    QTextStream in(stdin);
    JournalMonitor application(argc, argv);
    if (application.earlyStartupBringToImmediateHalt)
        return 1;
    return application.exec();
}
