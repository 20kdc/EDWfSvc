/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef EDSMTRANSIENTTAGGER_H
#define EDSMTRANSIENTTAGGER_H

#include <QObject>
#include <spaceboijsonvalue.h>

/*
 * Tags JSON objects according to the https://www.edsm.net/en/api-journal-v1 "Transient states" documentation,
 *  with added safety room, and allows access to the resulting information.
 */
class EDSMTransientTagger : public QObject {
    Q_OBJECT
public:
    explicit EDSMTransientTagger(QObject *parent = nullptr);

    void accept(SpaceBoiJsonValue & object);
    void tagEDSM(SpaceBoiJsonValue & object);
    void resetState();

    SpaceBoiJsonValue systemAddress = SpaceBoiJsonValue::Null;
    SpaceBoiJsonValue systemName = SpaceBoiJsonValue::Null;
    SpaceBoiJsonValue systemCoordinates = SpaceBoiJsonValue::Null;
    SpaceBoiJsonValue marketId = SpaceBoiJsonValue::Null;
    SpaceBoiJsonValue stationName = SpaceBoiJsonValue::Null;
    SpaceBoiJsonValue shipId = SpaceBoiJsonValue::Null;
    // -- Not Tagged --
    SpaceBoiJsonValue cmdr = SpaceBoiJsonValue::Null;
    SpaceBoiJsonValue stationEconomies = SpaceBoiJsonValue::Null;
    bool sendEvents = true;
};

#endif // EDSMTRANSIENTTAGGER_H
