/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef EDSMINTERFACE_H
#define EDSMINTERFACE_H

#include <spaceboijsonvalue.h>
#include <QObject>
#include <QSet>
#include <QTextStream>

#include "edsmtransienttagger.h"
#include "networksystem.h"

class EDSMInterface : public QObject {
    Q_OBJECT
public:
    // apiRoot = https://www.edsm.net/api-journal-v1
    // note the lack of '/'
    explicit EDSMInterface(QObject *parent, NetworkSystem * nsys, const QString & apiRoot, const QString & loginName, const QString & loginAPIKey);
    // WARNING: Do not call if upload is not ready!
    void flush();
public slots:
    void incomingEvent(SpaceBoiJsonValue & qjo, EDSMTransientTagger & tagger);
private slots:
    void onDiscardReply(void * userdata, bool success, QByteArray & response);
    void onSendReply(void * userdata, bool success, QByteArray & response);
signals:
    void readyForFirstUpload();
    void readyForNextUpload();
private:
    NetworkSystem * ns;
    QString apiRoot;
    QSet<QString> discardable;
    SpaceBoiJsonValue buffer;
    SpaceBoiJsonValue bufferMessage;
};

#endif // EDSMINTERFACE_H
