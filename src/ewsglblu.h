/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef EWSGLBLU_H
#define EWSGLBLU_H

#include <QString>
#include <QTextStream>

#define EDWF_BRAND "EDWfSvc"
#define EDWF_VERSION "v0.1"
// EDDN
#define EDWF_BRANDFULL EDWF_BRAND " " EDWF_VERSION
#define EDWF_JOURNALDB "EDWfKnownJournalData"

// #define DEBUG_DATA_SUBMIT
// #define DEBUG_DATA_SUBMIT_PRETEND
// #define DEBUG_FORGETFUL

static inline void edwfPrintln(const QString & qs) {
    QTextStream s(stderr);
    s << qs << endl;
}

#endif // EWSGLBLU_H
