/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef SPACEBOIJSONVALUE_H
#define SPACEBOIJSONVALUE_H

#include <memory>
#include <QString>
#include <QList>

#include "ewsglblu.h"

/*
 * Naming from : ZOYTfpHl0JA
 * ...Well *you* find a unique name for it!
 *  - 20kdc
 */
class SpaceBoiJsonValue {
public:
    static SpaceBoiJsonValue Null;
    static SpaceBoiJsonValue True;
    static SpaceBoiJsonValue False;
    static SpaceBoiJsonValue Object;
    static SpaceBoiJsonValue Array;

    explicit SpaceBoiJsonValue(const QString & primaryToken);
    explicit SpaceBoiJsonValue(const SpaceBoiJsonValue & k, const SpaceBoiJsonValue & v);
    // Replace usages with Object/Array when possible
    explicit SpaceBoiJsonValue(const QString & primaryToken, const QList<SpaceBoiJsonValue> & content);

    inline bool operator ==(const QString & other) const {
        return objectToken == other;
    }
    inline bool operator !=(const QString & other) const {
        return objectToken != other;
    }
    inline bool null() const {
        return objectToken == "null";
    }
    inline bool hasIV(const QString & key, SpaceBoiJsonValue * result) const {
        if (objectToken != "{")
            return false;
        for (const SpaceBoiJsonValue & kv : contents) {
            if (kv.objectToken != ":")
                continue;
            if (kv.contents[0] == key) {
                if (result)
                    *result = kv.contents[1];
                return true;
            }
        }
        return false;
    }
    inline void setIV(const QString & key, const SpaceBoiJsonValue & set) {
        if (objectToken != "{")
            return;
        for (SpaceBoiJsonValue & kv : contents) {
            if (kv.objectToken != ":")
                continue;
            if (kv.contents[0] == key) {
                kv.contents.removeLast();
                kv.contents.append(set);
                return;
            }
        }
        contents.append(SpaceBoiJsonValue(SpaceBoiJsonValue(key), set));
    }
    inline void rmIV(const QString & key) {
        if (objectToken != "{")
            return;
        int contentsSize = contents.size();
        for (int i = 0; i < contentsSize; i++) {
            // Do not borrow; data may be removed
            SpaceBoiJsonValue kv = contents[i];
            if (kv.objectToken != ":")
                continue;
            if (kv.contents[0] == key) {
                contents.removeAt(i);
                return;
            }
        }
    }
    void rmLocalised();

    // Will result in Null on failure (used in EDSMTransientTagger logic)
    inline SpaceBoiJsonValue operator [](const QString & other) const {
        SpaceBoiJsonValue result = SpaceBoiJsonValue::Null;
        hasIV(other, &result);
        return result;
    }

    static SpaceBoiJsonValue parse(QByteArray & json, const char ** err);
    void writeString(QString & basis) const;
    inline QString writeString() const {
        QString base = "";
        writeString(base);
        return base;
    }
    QByteArray write() const;

    // For strings, this is "<string text> without any escapes. Note the lack of ending '"'.
    // For other kinds of token, this is just the raw token text.
    QString objectToken;
    // '{' contains ':'-type values.
    // '[' simply contains the values.
    // ':' contains two values (key & value).
    QList<SpaceBoiJsonValue> contents;
};

#endif // SPACEBOIJSONVALUE_H
