/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef NETWORKSYSTEM_H
#define NETWORKSYSTEM_H

#include <QObject>
#include <QUuid>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QHash>
#include "spaceboijsonvalue.h"

class NetworkSystemResponder : public QObject {
    Q_OBJECT
public:
    explicit inline NetworkSystemResponder(QNetworkReply * reply, QByteArray uplData, void * userdat, QObject * targetObject, const char * targetSignal) : QObject(reply), uploadData(uplData) {
        uid = QUuid::createUuid().toString();
        rpl = reply;
        userdata = userdat;
        if (!reply) {
            deleteLater();
        } else {
            connect(reply, SIGNAL(finished()), this, SLOT(rplFinished()));
            connect(reply, SIGNAL(destroyed(QObject *)), this, SLOT(rplDestroyed(QObject *)));
        }
        if (targetObject)
            connect(this, SIGNAL(rplExport(void *, bool, QByteArray &)), targetObject, targetSignal);
    }
    ~NetworkSystemResponder();
    QByteArray uploadData;
    QString uid;
signals:
    void rplExport(void * userdata, bool success, QByteArray & response);
private slots:
    void rplFinished();
    void rplDestroyed(QObject * ign);

private:
    bool shuttingDown = false;
    QNetworkReply * rpl;
    void * userdata;
};

/*
 * A wrapper around QNetworkAccessManager that logs errors,
 *  and allows better handling of per-transaction state.
 */
class NetworkSystem : public QObject {
    Q_OBJECT
public:
    explicit NetworkSystem(QObject *parent = nullptr);
    void get(const QString & target, void * userdata, QObject * targetObject, const char * targetSignal);
    // Expects a slot of type (void * userdata, bool success, QByteArray & response)
    void postJson(const QString & target, const SpaceBoiJsonValue & data, void * userdata, QObject * targetObject, const char * targetSignal);
private:
    QNetworkAccessManager * coreNetwork;
};

#endif // NETWORKSYSTEM_H
