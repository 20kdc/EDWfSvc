/*
 * EDWfSvc - Data uploader
 * Written starting in 2018 by contributors (see README.md)
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <QDir>
#include <QFile>
#include "ewsglblu.h"
#include "networksystem.h"

NetworkSystem::NetworkSystem(QObject *parent) : QObject(parent) {
    coreNetwork = new QNetworkAccessManager(this);
}

static void outputDump(const QString & prefix, const QByteArray & data) {
    QDir qdir(".");
    qdir.mkdir("debug");
    QFile dbg(QString("debug") + QDir::separator() + prefix);
    dbg.open(QFile::WriteOnly);
    dbg.write(data);
}

void NetworkSystem::get(const QString & target, void * userdata, QObject * targetObject, const char * targetSignal) {
#ifndef DEBUG_DATA_SUBMIT_PRETEND
    QNetworkRequest req(target);
    req.setHeader(QNetworkRequest::KnownHeaders::UserAgentHeader, EDWF_BRANDFULL);
    QNetworkReply * rq = coreNetwork->get(req);
    new NetworkSystemResponder(rq, target.toUtf8(), userdata, targetObject, targetSignal);
#else
    new NetworkSystemResponder(nullptr, target.toUtf8(), userdata, targetObject, targetSignal);
#endif
}
// Expects a slot of type (void * userdata, bool success, QByteArray & response)
void NetworkSystem::postJson(const QString & target, const SpaceBoiJsonValue & data, void * userdata, QObject * targetObject, const char * targetSignal) {
    NetworkSystemResponder * nsr;
    QByteArray qba = data.write();
#ifndef DEBUG_DATA_SUBMIT_PRETEND
    QNetworkRequest req(target);
    req.setHeader(QNetworkRequest::KnownHeaders::UserAgentHeader, EDWF_BRANDFULL);
    req.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader, "application/json");
    QNetworkReply * rq = coreNetwork->post(req, qba);
    nsr = new NetworkSystemResponder(rq, qba, userdata, targetObject, targetSignal);
#else
    nsr = new NetworkSystemResponder(nullptr, qba, userdata, targetObject, targetSignal);
#endif
#ifdef DEBUG_DATA_SUBMIT
    outputDump("Upload-" + nsr->uid, nsr->uploadData);
#endif
}

NetworkSystemResponder::~NetworkSystemResponder() {
    if (shuttingDown)
        return;
    shuttingDown = true;
    QByteArray qba = QByteArray();
    emit rplExport(userdata, false, qba);
}

void NetworkSystemResponder::rplFinished() {
    if (shuttingDown)
        return;
    shuttingDown = true;
    QByteArray qba = this->rpl->readAll();
    bool success = this->rpl->error() == QNetworkReply::NoError;
    if (!success) {
        // Output dump file
        outputDump("Upload-" + uid, uploadData);
        outputDump("Download-" + uid, qba);
        edwfPrintln("Failed to run request. Saved to debug folder as: " + uid);
    }
    emit rplExport(userdata, success, qba);
    // Will cause rplDestroyed which will cause full shutdown
    this->rpl->deleteLater();
}
void NetworkSystemResponder::rplDestroyed(QObject * ign) {
    deleteLater();
}
